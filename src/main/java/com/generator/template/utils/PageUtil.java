package com.generator.template.utils;

import cn.hutool.core.util.StrUtil;

/**
 * 分页工具类
 */
public class PageUtil {

    /**
     * 判断是否分页
     * @param page
     * @param limit
     * @return
     */
    public static Integer setPage(Integer page, Integer limit) {
        if (!StrUtil.isBlankIfStr(page) && !StrUtil.isBlankIfStr(limit)) {
            return (page - 1) * limit;
        }
        return null;
    }

}
