package com.generator.template;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackages = {"com.generator.template.*"})
@MapperScan("com.generator.template.mapper")
@EnableScheduling
public class GeneratorTemplate {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorTemplate.class, args);
    }
}
