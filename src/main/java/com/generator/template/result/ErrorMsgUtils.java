package com.generator.template.result;

import com.generator.template.aop.Desc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class ErrorMsgUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorMsgUtils.class);

    public ErrorMsgUtils() {
    }

    public static Map<String, String> scanErrorMessages(Class clazz) {
        HashMap map = new HashMap();

        try {
            Field[] fields = clazz.getDeclaredFields();
            Field[] var3 = fields;
            int var4 = fields.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Field f = var3[var5];
                if (Modifier.isPublic(f.getModifiers()) && f.getType() == String.class) {
                    String name = null;
                    Desc desc = (Desc)f.getAnnotation(Desc.class);
                    if (desc != null) {
                        name = desc.value();
                    }

                    map.put((String)f.get((Object)null), name);
                }
            }

            return map;
        } catch (Exception var9) {
            LOGGER.error("ErrorMessage init failed, class is :{}, cause : {}", clazz.getName(), var9);
            throw new IllegalStateException("ErrorMessage init failed", var9);
        }
    }
}