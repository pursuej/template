package com.generator.template.result;

public class RtnMessage {
    private String msgText;
    private String msgCode;

    public RtnMessage(String msgText, String msgCode) {
        this.msgText = msgText;
        this.msgCode = msgCode;
    }

    public RtnMessage() {
    }

    public String getMsgText() {
        return this.msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getMsgCode() {
        return this.msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }
}
