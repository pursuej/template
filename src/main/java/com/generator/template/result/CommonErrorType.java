package com.generator.template.result;



import com.generator.template.aop.Desc;

import java.util.HashMap;
import java.util.Map;

public class CommonErrorType {
    public static final int BASE_MODULE = 0;
    @Desc("系统错误，请联系管理员")
    public static final String ERR = "10000";
    @Desc("操作成功")
    public static final String SUC = "10001";
    @Desc("操作失败")
    public static final String FAIL = "10002";
    @Desc("操作异常")
    public static final String OP_ERR = "10003";
    @Desc("参数异常")
    public static final String PARAM_ERR = "10004";
    @Desc("未登录或登录超时")
    public static final String NO_LOGIN = "10005";
    @Desc("未授权，请联系管理员")
    public static final String NO_AUTHORITY = "10006";
    @Desc("服务配置错误，请联系管理员")
    public static final String CONFIG_ERR = "10007";
    @Desc("服务调用失败，请联系管理员")
    public static final String RESP_ERR = "10008";
    @Desc("微服务响应异常，请联系管理员")
    public static final String FEIGN_RESP_ERROR = "10010";
    public static Map<Integer, Map<String, String>> errorMessages = new HashMap();

    public CommonErrorType() {
    }

    public static String getErrorCode(int module, int subModule, int code) {
        return String.valueOf(module * 1000000 + subModule * 1000 + code);
    }

    public static void registerErrorMessages(int module, Map<String, String> moduleErrorMessages) {
        errorMessages.put(module, moduleErrorMessages);
    }

    public static String getErrorMessage(String code) {
        return code.length() != 9 ? (String)((Map)errorMessages.get(0)).get(code) : (String)((Map)errorMessages.get(Integer.parseInt(code) / 1000000)).get(code);
    }

    static {
        Map<String, String> baseErrorMessages = ErrorMsgUtils.scanErrorMessages(CommonErrorType.class);
        registerErrorMessages(0, baseErrorMessages);
    }
}

