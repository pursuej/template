package com.generator.template.result;


import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ResultDto<E> {
    private int code;
    private List<RtnMessage> msg;
    private E data;
    private Integer total;

    public ResultDto() {
    }

    public ResultDto(int code, RtnMessage rtnMessage, E obj) {
        this.code = code;
        this.msg = Collections.singletonList(rtnMessage);
        this.data = obj;
    }

    public ResultDto(int code, RtnMessage rtnMessage, E obj, Integer total) {
        this.code = code;
        this.msg = Collections.singletonList(rtnMessage);
        this.data = obj;
        this.total = total;
    }

    public ResultDto(int code, RtnMessage msg) {
        this.code = code;
        this.msg = Collections.singletonList(msg);
    }

    public boolean success() {
        return this.code == 1;
    }

    public boolean fail() {
        return this.code != 1;
    }

    public ResultDto(int code, List<RtnMessage> msg) {
        this.code = code;
        this.msg = msg;
    }

    public static <T> ResultDto<T> buildForSuccess(T obj) {
        return new ResultDto(200, new RtnMessage(CommonErrorType.getErrorMessage("10001"), "10001"), obj);
    }

    public static <T> ResultDto<T> buildForSuccess(T obj, Integer total) {
        return new ResultDto(200, new RtnMessage(CommonErrorType.getErrorMessage("10001"), "10001"), obj, total);
    }

    public static ResultDto buildForParamError(Set<String> errorField) {
        return new ResultDto(0, new RtnMessage(CommonErrorType.getErrorMessage("10004"), "10004"), errorField);
    }

    public static <T> ResultDto<T> buildForErrorCode(String errorCode) {
        return new ResultDto(0, new RtnMessage(CommonErrorType.getErrorMessage(errorCode), errorCode));
    }

    public static <T> ResultDto<T> buildForErrorCode(String errorCode, String errorMessage) {
        return new ResultDto(0, new RtnMessage(errorMessage, errorCode));
    }

    public static <T> ResultDto<T> buildForErrorCode(String errorCode, String errorMessage, T obj) {
        return new ResultDto(0, new RtnMessage(errorMessage, errorCode), obj);
    }

    public static <T> ResultDto<T> buildForSuccess() {
        return new ResultDto(1, new RtnMessage(CommonErrorType.getErrorMessage("10001"), "10001"));
    }

    public static <T> ResultDto<T> buildForError() {
        return new ResultDto(0, new RtnMessage(CommonErrorType.getErrorMessage("10002"), "10002"));
    }

    public static <T> ResultDto<T> buildForParamError(String message) {
        return new ResultDto(0, new RtnMessage(message, "10004"));
    }

    public static <T> ResultDto<T> buildForParamError() {
        return new ResultDto(0, new RtnMessage(CommonErrorType.getErrorMessage("10004"), "10004"));
    }

    public static <T> T rebuildSuccess(ResultDto<T> resultDTO) {
        return resultDTO.getData() != null ? resultDTO.getData() : null;
    }

    public static List<RtnMessage> rebuildError(ResultDto resultDTO) {
        return resultDTO.getMsg();
    }

    public <T> ResultDto<T> returnError() {
        return new ResultDto(this.code, this.msg);
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<RtnMessage> getMsg() {
        return this.msg;
    }

    public void setMsg(List<RtnMessage> msg) {
        this.msg = msg;
    }

    public E getData() {
        return this.data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}

